# Build
FROM rust:slim AS build
WORKDIR /app
COPY . .
RUN cargo build --release

ENV APP_HOME=/app
WORKDIR $APP_HOME

#Expose port
EXPOSE 3000

#Run app
CMD cargo run