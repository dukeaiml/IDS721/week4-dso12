/*
write aactix web app that displays a greeeting with input name
*/

use actix_web::{web, App, HttpServer, HttpResponse, Responder};

async fn index() -> impl Responder {
    HttpResponse::Ok()
        .content_type("text/html")
        .body(
            r#"
            <html>
            <head><title>Rust Actix Password Checker</title></head>
            <body>
                <h1>Rust Actix Password Checker</h1>
                <p>This web app checks whether a password is strong based on specific criteria.</p>
                <p>To use this app, send a GET request to the following endpoint:</p>
                <code>GET /check_password/your_password_here</code>
                <p>The app will respond with either 'Strong' or 'Weak' depending on the strength of the password.</p>
            </body>
            </html>
            "#,
        )
}

async fn check_password(password: web::Path<String>) -> impl Responder {
    let password = password.into_inner();
    let mut reasons = Vec::new();

    if password.len() < 8 {
        reasons.push("It has less than 8 characters.".to_string());
    }

    if !password.chars().any(|c| c.is_lowercase()) {
        reasons.push("It does not contain any lowercase letters.".to_string());
    }

    if !password.chars().any(|c| c.is_uppercase()) {
        reasons.push("It does not contain any uppercase letters.".to_string());
    }

    if !password.chars().any(|c| c.is_digit(10)) {
        reasons.push("It does not contain any digits.".to_string());
    }

    let special_characters = "!@#$%^&*()-+";
    if !password.chars().any(|c| special_characters.contains(c)) {
        reasons.push("It does not contain any special characters.".to_string());
    }

    for window in password.chars().collect::<Vec<_>>().windows(2) {
        if window[0] == window[1] {
            reasons.push("It contains two of the same character in adjacent positions.".to_string());
            break;
        }
    }

    if reasons.is_empty() {
        HttpResponse::Ok().body(format!("'{}' is a strong password!", password))
    } else {
        HttpResponse::Ok().body(format!("'{}' is weak password because {}", password, reasons.join(" ")))
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(index))
            .route("/check_password/{password}", web::get().to(check_password))
    })
    .bind("0.0.0.0:3000")? //change binding to allow external connections
    .run()
    .await
}

