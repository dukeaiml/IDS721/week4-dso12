# Rust Actix Web App Containerization

## Overview
This repository contains a simple Rust Actix web application that checks the strength of passwords based on specific criteria.

## Password Criteria
The password strength checking criteria are as follows:

* It must have at least 8 characters.
* It must contain at least one lowercase letter.
* It must contain at least one uppercase letter.
* It must contain at least one digit.
* It must contain at least one special character from the following string: "!@#$%^&*()-+".
* It must not contain two of the same characters in adjacent positions (e.g., "aab" violates this condition, but "aba" does not).

## Requirements
* Docker
* Rust (if you want to run the application locally without Docker)

## Running the Application
### Using Docker

1. Clone this repository to your local machine.
2. Navigate to the root directory of the cloned repository.
3. Build the Docker image using the provided Dockerfile:
` docker build -t rust-actix-app .`
4. Run the Docker container:
`docker run -p 3000:3000 rust-actix-app`
5. Access the web application by opening a web browser and navigating to http://localhost:3000.
6. To stop the running container, open a new terminal window/tab and run:
`docker ps`
This command will list all running containers along with their IDs. Note the container ID of the rust-actix-app container.
7. Once you have the container ID, stop the container by running:
`docker stop <container_id>`
8. Optionally, if you want to remove the container after stopping it, you can run:
`docker rm <container_id>`

### Running Locally (without Docker)

1. Ensure you have Rust installed on your machine. If not, you can install it from rustup.rs.
2. Clone this repository to your local machine.
3. Navigate to the root directory of the cloned repository.
4. Build and run the application using Cargo:
`cargo run`
5. Access the web application by opening a web browser and navigating to http://localhost:3000.

### Testing with curl
You can test the functionality of the application using curl:

To check the password strength, run:
`curl http://localhost:3000/check_password/your_password_here`.

Replace "your_password_here" with the password you want to test.

## Screenshots
![docker running](/images/docker.png)
![docker running](/images/web.png)
![docker running](/images/weak.png)

## Author
dso12@duke.edu